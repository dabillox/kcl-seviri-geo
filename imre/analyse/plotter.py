# -*- coding: utf-8 -*-
"""
.. module:: plot
   :platform: mac, linux
   :synopsis: contains the tools to perform the
   analysis of the outputs of the co-registration

.. moduleauthor:: Daniel Fisher <daniel.fisher@kcl.ac.uk>

"""

import logging
logger = logging.getLogger(__name__)

import numpy as np
import cv2
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap 
import numpy.ma as ma

# Useful functions-----------------------------------------------------
    

def convert_to_grayscale(image):
    if np.min(image == 0) and np.max(image == 255):
        print 'image already grayscale'
        return image
    else:
        minPixelValue = np.min(image) * 1.0
        maxPixelValue = np.max(image) * 1.0
        imageGrayScale = ((image + minPixelValue)/maxPixelValue)*255
        return imageGrayScale
    
    
def draw_circle(c, r):
    t = np.arange(0,1.01,.01)*2*np.pi
    x = r*np.cos(t) + c[0]
    y = r*np.sin(t) + c[1]
    plt.plot(x,y,'b',linewidth=2)
    

def appendimages(im1,im2):
    """ Return a new image that appends the two images side-by-side. """
    # select the image with the fewest rows and fill in enough empty rows
    rows1 = im1.shape[0]
    rows2 = im2.shape[0]
    if rows1 < rows2:
        im1 = np.concatenate((im1,np.zeros((rows2-rows1,im1.shape[1]))),axis=0)
    elif rows1 > rows2:
        im2 = np.concatenate((im2,np.zeros((rows1-rows2,im2.shape[1]))),axis=0)
    # if none of these cases they are equal, no filling needed.
    return np.concatenate((im1,im2), axis=1)


def hist_eq(im,nbr_bins=256):
    imhist,bins = np.histogram(im.flatten(),nbr_bins,normed=True)
    cdf = imhist.cumsum() #cumulative distribution function
    cdf = 255 * cdf / cdf[-1] #normalize
    im2 = np.interp(im.flatten(),bins[:-1],cdf)
    return im2.reshape(im.shape).astype('uint8')


# ---------------------------------------------------------------------



# Plotting ------------------------------------------------------------
def plot_features(locs, 
                  im):
    '''
    Plot the extracted features on an image.
    '''
    im = ma.masked_array(im, im <= 0)
    plt.imshow(im, cmap='gray')
    plt.plot(locs[:,0],locs[:,1], 'co', markersize=4)
    plt.axis('off')
    plt.show()
    plt.close()
    

def plot_matches(im1,im2,tp, cp,
                 locs1,locs2,show_below=False, pltLim=1000):
    '''
    Plot the matched between the two images.
    '''
    im3 = appendimages(convert_to_grayscale(im1),convert_to_grayscale(im2))
    
    im3 = ma.masked_array(im3, im3 == 0)

    
    if show_below:
        im3 = np.vstack((im3,im3))
        
    plt.imshow(im3, cmap="gray")
    cols1 = im1.shape[1]
    plt.plot([locs1[tp,0],locs2[tp,0]+cols1],
             [locs1[tp,1],locs2[tp,1]],'r')
    plt.plot([locs1[cp,0],locs2[cp,0]+cols1],
             [locs1[cp,1],locs2[cp,1]],'b')
    plt.axis('off')
    plt.show()
    plt.close()


def plot_tiepoints(locs, tp, cp, im):
    '''
    Plot the tie-points between the two images.
    '''
    
    im = ma.masked_array(im, im <= 0)
    
    plt.imshow(im, cmap='gray')
    plt.plot(locs[tp,0],locs[tp,1],'ro', markersize=4, label="Tiepoints")
    plt.plot(locs[cp,0],locs[cp,1],'bo', markersize=4, label="Checkpoints")
    plt.legend(numpoints=1)
    plt.axis('off')
    plt.show()
    plt.close()


def plot_warp(im):
    '''
    Plot the warped image.
    '''
    im = ma.masked_array(im, im <= 0)
    plt.imshow(im, cmap='gray')
    plt.show()
    plt.close()


def get_flow(ref, warped_comp):
    '''
    Get the optical flow between 
    the two images
    '''
    
    ref = hist_eq(ref)
    comp = hist_eq(warped_comp)
    flow = cv2.calcOpticalFlowFarneback(comp, ref, 0.5, 4, 30, 7, 7, 1.5, 0)
    return flow

def plot_flow(flow, ref_mask):
    '''
    Plot the optical flow between the two images
    '''
    fx = flow[...,0]
    fy = flow[...,1]
    

    fx = ma.masked_array(fx, ~ref_mask)
    fy = ma.masked_array(fy, ~ref_mask)
    
    plt.imshow(fx, vmin = -5, vmax=5)
    cbar = plt.colorbar()
    plt.show()
    plt.close()
    
    plt.imshow(fy, vmin=-5, vmax=5)
    cbar = plt.colorbar()
    plt.show()
    plt.close()


def plot_hist(x_flow,
              y_flow):
    '''
    Plot 2D the histograms of the optical flow.
    x axis East West shifts
    y axis North South shifts
    '''
    pass
# ---------------------------------------------------------------------


def make_plots(ref_locs,
               comp_locs,
               data_dict,
               warped_comp,
               tie_points,
               check_points):
    '''
    Runs the analysis on the 
    '''
    
    # set up the map
    lons = data_dict['lons']
    lats = data_dict['lats']
    ref = data_dict['ref_arr']
    comp = data_dict['comp_arr']
    warped_comp = data_dict['warped_comp']
    ref_mask = data_dict['ref_mask']
    
    # flip the imagery ud
    #ref = np.flipud(ref)
    #comp = np.flipud(comp)
    #warped_comp = np.flipud(warped_comp)
    
    plot_features(ref_locs, ref)
    plot_features(comp_locs, comp)

    plot_matches(ref,comp, tie_points, check_points, ref_locs,comp_locs)
    plot_tiepoints(ref_locs, tie_points, check_points, ref)
    plot_warp(warped_comp)
    flow = get_flow(ref, warped_comp)
    plot_flow(flow, ref_mask)
    #plot_hist(flow, ref_mask)






