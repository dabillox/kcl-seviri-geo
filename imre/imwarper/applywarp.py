'''
.. module:: _apply_warp
   :platform: mac, linux
   :synopsis: apply warp

.. moduleauthor:: Daniel Fisher <daniel.fisher@kcl.ac.uk>
'''

import numpy as np


def make_grids(shp):
    return np.meshgrid(np.arange(shp[1]), np.arange(shp[0]))


def make_resample(map_shape,
                  transformation):
    
    #make the image coordinate grid
    gridX, gridY = make_grids(map_shape)

    
    #get new image coordinates
    warpedGridY = transformation[0] + gridY*transformation[1] + gridX*transformation[2] 
    warpedGridX = transformation[3] + gridY*transformation[4] + gridX*transformation[5]
    
    #round NN coordintes
    warpedGridY = np.round(warpedGridY).astype('int')
    warpedGridX = np.round(warpedGridX).astype('int')
    
    #find the inbound map points
    in_y = (warpedGridY >= 0) & (warpedGridY < map_shape[0])
    in_x = (warpedGridX >= 0) & (warpedGridX < map_shape[1])
    index =  in_y & in_x
    
    #obtain the inbound map points
    y = np.squeeze(warpedGridY[index])
    x = np.squeeze(warpedGridX[index])
    
    #obtain the inbound image point
    r = np.squeeze(gridY[index])
    c = np.squeeze(gridX[index])
    
    #return resampling coords
    return x,y,r,c
    

def resample(x,y,r,c,
             image,
             NULL_VALUE):
    
    #create the output image
    resampledImage = np.zeros(image.shape) + NULL_VALUE
    
    resampledImage[y,x] = image[r,c]
    return resampledImage

def warp(data_dict,
          transformation,
          args_dict):
    
    image = data_dict['comp_arr']
    mask = data_dict['comp_mask']
    map_shape = data_dict['ref_arr'].shape
    NULL_VALUE = args_dict['NULL_VALUE']

    
    x,y,r,c = make_resample(map_shape,
                             transformation)
    
    resampled_image = resample(x,y,r,c,
                               image,
                               NULL_VALUE)
    
    resampled_mask = resample(x,y,r,c,
                              mask, 0)
    
    return resampled_image, resampled_mask.astype('bool')