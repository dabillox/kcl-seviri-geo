'''
.. module:: derive_and_apply
   :platform: mac, linux
   :synopsis: derive and applies the warp to the image pair

.. moduleauthor:: Daniel Fisher <daniel.fisher@kcl.ac.uk>

'''

import logging
logger = logging.getLogger(__name__)

import derivewarp
import applywarp

import numpy as np

def tests():
    pass


def derive_apply(locs1, 
                 locs2, 
                 data_dict,
                 args_dict):
    
    transformation, tie_points, check_points, rmse = derivewarp.derive(locs1,
                                                                       locs2,
                                                                       args_dict)
    warped_im, warped_mask = applywarp.warp(data_dict,
                                            transformation,
                                            args_dict)
    
    return warped_im, warped_mask, tie_points, check_points, rmse