'''
.. module:: _derive_warp
   :platform: mac, linux
   :synopsis: derive warp using LSM

.. moduleauthor:: Daniel Fisher <daniel.fisher@kcl.ac.uk>
'''

import numpy as np
import scipy.linalg as linalg


import logging
logger = logging.getLogger(__name__)

#--------------------------------------------------------------------------------------------

def round_to_base(values_to_round, bucket_size):
    return bucket_size*np.round(values_to_round/bucket_size)


def buckets(locs1, bucket_size):
    
    sampling_dict = {}
    
    #tie points are x,y rather than usual numpy y,x convention
    x_max = np.round(np.max(locs1[:,0])) 
    x_min = np.round(np.min(locs1[:,0]))
    y_max = np.round(np.max(locs1[:,1]))
    y_min = np.round(np.min(locs1[:,1]))
        
    # find the (whole) number of buckets in the region defined by the
    # tie-point for both x and y directions.  x_bucket_count is the 
    # total number of whole buckets that fit in to the x-axis (although 
    # the final bucket is sometimes fractional, we add one in these instances!).  
    # same process for y  
    x_diff = x_max - x_min
    y_diff = y_max - y_min
    x_rem = x_diff % bucket_size
    y_rem = y_diff % bucket_size
    if x_rem == 0:
        total_x_buckets = x_diff/bucket_size
    else:
        total_x_buckets = (x_diff - x_rem)/bucket_size + 1 #add one more bucket as we have subtracted the remainder 
    if y_rem == 0:
        total_y_buckets = y_diff/bucket_size
    else:
        total_y_buckets = (y_diff - y_rem)/bucket_size + 1
                        
    # now we need to find the unique sampled buckets.  Which is done by round each bucket
    # to its bucket_size.  So, if we have a bucket size of 2^4 (which is a side of 16), then 
    # each bucket index will be rounded to the nearest base 2 number at the quantisations of 
    # the given bucket size, i.e. 0, 16, 32, 64 etc.  For a bucket size of 2^5 (which is a side
    # of 32) the rounding is as follows: 0, 34, 64, 128.  EAch index if there rounded to its
    # nearest bucket.  We then just have to find the unique bucket combinations of x and y 
    # positions, which gives us the total number of sampled buckets. easiest way to do it
    # is with a lists and sets, but is ugly...
    x_bucket_sampled = round_to_base(np.round(locs1[:,0]) - x_min, bucket_size)
    y_bucket_sampled = round_to_base(np.round(locs1[:,1]) - y_min, bucket_size)
    
    # BIT UGLY but does converts arrays to lists, gets the set  giving the unique tuples and
    # then gets it length
    sampled_buckets = len(set(zip(list(x_bucket_sampled),list(y_bucket_sampled))))
                     
    # populate sampling dict
    sampling_dict['y_buckets_sampled'] = y_bucket_sampled 
    sampling_dict['total_y_buckets'] = total_y_buckets
    sampling_dict['x_buckets_sampled'] = x_bucket_sampled
    sampling_dict['total_x_buckets'] = total_x_buckets
    sampling_dict['sampled_buckets'] = sampled_buckets
    sampling_dict['bucket_size'] = bucket_size
    
    return sampling_dict


def estimate(sampling_dict, PROB):
    '''
    Determine number of trials to get to .99% confidence that optimum sample located
    '''
    
    sampled_buckets = sampling_dict['sampled_buckets']
    total_buckets = sampling_dict['total_y_buckets'] * sampling_dict['total_x_buckets']
                            
    expo = total_buckets/sampled_buckets
    frac = sampled_buckets/total_buckets
    
    numerator = np.log(PROB) 
    denominator = np.log(1 - frac**expo)
    
    if denominator == 0:
        logger.warning("denominator in trial calculation is 0 - trying again")
        return -1
    
    trials = np.round( numerator / denominator)
    return trials

def estimate_trials(locs1, args_dict):
    
    
    # set up arguments
    bucket_count = args_dict['BUCKET_INIT_SIZE']
    n_trials = args_dict['TRIALS']
    desired_trials = args_dict['TRIALS']
    MAX_BUCKET_SIZE = args_dict['MAX_BUCKET_SIZE']
    PROB = args_dict['TRIAL_PROBABILITY']
    
    
    #find suitable bucket size
    while ((n_trials >= desired_trials) | (n_trials < 0)): 
        
        # bucket size is of base 2
        bucket_size = 2 ** bucket_count
        bucket_count+=1
    
        # why 512 in this instance, maybe valid for some images but not all
        # TODO - change it to be more adaptable at some point
        if bucket_size == MAX_BUCKET_SIZE:
            logging.critical('max bucket_size exceeded.  Estimated iterations was %f' % n_trials)
            return -1
        
        #divide the image up into buckets to aid tie point selection
        sampling_dict = buckets(locs1, bucket_size)
    
        #determine the number of trials to find optimum based on sampling distribution
        n_trials = estimate(sampling_dict, PROB)
        print "n trials", n_trials
        print "check ", ((n_trials >= desired_trials) | (n_trials < 0))
        print ''
    
    logging.debug("Applied bucket size is %f4" % bucket_size)
    return n_trials.astype('int'), sampling_dict

#--------------------------------------------------------------------------------------------

def select_tp(locs1, d):
    '''
    find the bucket to which each tiepoint belongs
    and define the weight (sample ratio) for the bucket
    '''    
    
    bucket_index = np.zeros(locs1.shape[0])  # defines which bucket each tie-point is in
    bucket_weights = np.zeros(d['sampled_buckets']) # dfines the number tie-points in each bucket
    
    # so here we need to select the tie-points from the available matched feature pairs.
    # We only want to select one tie-point per bin, all other features in the bin can
    # be used as check point to validate the warp.  Furthermore, we correct for selection biases
    # due to some bins having more/less tie points than others.
    
    
    # First we loop over all of the potential buckets at the quantisation of the bucket size.
    # We then check if the current bucket has tiepoints contained within it  
    bucket_id = 0  
    for i in np.arange(0, (d['total_y_buckets'] + 1) * d['bucket_size'], d['bucket_size']): 
        for j in np.arange(0, (d['total_x_buckets'] + 1) * d['bucket_size'], d['bucket_size']):
            current_bucket_tps = (d['y_buckets_sampled'] == i) & (d['x_buckets_sampled'] == j)
            if np.sum(current_bucket_tps):  #if there are samples then sum will be > 0 and therefore True
                if bucket_id >= d['sampled_buckets']:
                    logger.warning("More buckets than have been sampled, this should not happen")
                    continue
                bucket_index[current_bucket_tps] = bucket_id 
                bucket_weights[bucket_id] = np.sum(current_bucket_tps) #this sums the total number of tie-points within the given bucket
                bucket_id += 1
    
    #scale the bucket weights from 0,1 and cumulatively sum
    bucket_weights = np.cumsum(bucket_weights * (1.0/np.sum(bucket_weights)))
    
    #randomly select a bucket
    tiepointIndexes = None
    checkpointIndexes = None
    while np.min(bucket_weights) < 99999:
        randomNumber = np.random.random()
        chosenBucket = np.argmin(np.abs(bucket_weights - randomNumber))
            
        #randomly select a tiepoint
        potentialTiepoints = np.where(bucket_index == chosenBucket)[0]
        if potentialTiepoints.size > 1:
            chooser = np.zeros(potentialTiepoints.size, dtype='bool') 
            chosenTiepoint = np.argmin(np.random.random(potentialTiepoints.size))
            chooser[chosenTiepoint] = 1  
        
        #extract tiepoint index from bin
        if tiepointIndexes == None:
            if potentialTiepoints.size == 1:
                tiepointIndexes = potentialTiepoints
            else:
                tiepointIndexes = potentialTiepoints[chooser]
        else:
            if potentialTiepoints.size == 1:
                tiepointIndexes = np.concatenate([tiepointIndexes, potentialTiepoints], axis=0)
            else:
                tiepointIndexes = np.concatenate([tiepointIndexes, potentialTiepoints[chooser]], axis=0)
            
        #set any remaining tie points in the bin to check points
        if potentialTiepoints.size > 1:
            if checkpointIndexes == None:
                checkpointIndexes = potentialTiepoints[~chooser]
            else:
                checkpointIndexes = np.concatenate([checkpointIndexes, potentialTiepoints[~chooser]], axis=0)
            
        #remove the chosen bucket from further consideration
        bucket_weights[chosenBucket] = 99999
          
    return tiepointIndexes, checkpointIndexes


def lsfit(locs1, locs2, index):
    '''
    This deermines the transformation to warp the image 
    onto the map
    '''
    #regressand -  the 'map' coordinates, which are dependent upon the 'image' 
    R = np.zeros([index.size*2]) 
    R[0:index.shape[0]] =  locs2[index,1] #map indexes (y)
    R[index.shape[0]:] = locs2[index,0] #map indexes (x)
        
    #design - the 'image' coordinates
    inputD = np.zeros([index.size, 3])
    inputD[:,0] = 1
    inputD[:,1] = locs1[index,1] #image indexes (r)
    inputD[:,2] = locs1[index,0] #image indexes (c)
        
    D = np.zeros([index.size*2, 6])
    D[0:index.size, 0:3] = inputD
    D[index.size:,3:] = inputD 
                    
    #derive the function to transform the image coordinates (X) onto the map (Y), so that Y=f(X)
    DT = D.T 
    DTD = np.dot(DT,D)
    DTR = np.dot(DT, R)
    L,lower = linalg.cho_factor(DTD, lower=True)
    return linalg.cho_solve((L,lower),DTR) 


def remove_outliers(locs1, locs2, tp_index, transform):
    
    diffX, diffY = apply_warp(locs1, locs2, tp_index, transform)
     
    meanDiffY = np.mean(diffY) 
    meanDiffX = np.mean(diffX)
    stdDiffY = np.std(diffY)
    stdDiffX = np.std(diffX)
    
    y_min_lim = diffY > (meanDiffY - 3*stdDiffY)
    y_max_lim = diffY < (meanDiffY + 3*stdDiffY)
    x_min_lim = diffX > (meanDiffX - 3*stdDiffX)
    x_max_lim = diffX < (meanDiffX + 3*stdDiffX)
    inliers = y_min_lim & y_max_lim & x_min_lim & x_max_lim
    tp_index = tp_index[inliers]
    
    return tp_index
    

def warp_stats(locs1, locs2, tp_index, cp_index, transform):

    diffX, diffY = apply_warp(locs1, locs2, tp_index, transform)

    #evaluate tiepoints
    residuals = np.sqrt(diffY**2 + diffX**2)
    sumSquareResiduals  = np.sum(residuals**2)
    tiepointRMSE = np.sqrt(sumSquareResiduals/diffY.shape[0]) 
        
    #warp checkpoints
    warpedY, warpedX = warp_points(locs1, cp_index, transform)
    
    #evaluate checkpoints
    diffX = warpedX - locs2[cp_index,0]
    diffY = warpedY - locs2[cp_index,1]
    residuals = np.sqrt(diffY**2 + diffX**2)
    sumSquareResiduals  = np.sum(residuals**2)
    checkpointRMSE = np.sqrt(sumSquareResiduals/diffY.shape[0]) 
            
    #compute RMSE
    #RMSE = tiepointRMSE + checkpointRMSE
    
    return checkpointRMSE
    

def apply_warp(locs1, locs2, tp_index, transform):
    
    #apply warp to the 'image'
    warpedY, warpedX = warp_points(locs1, tp_index, transform)
             
    #compute distances of the warped 'image' coordinates from the 'map' coordinates 
    diffX = warpedX - locs2[tp_index,0]
    diffY = warpedY - locs2[tp_index,1]
    
    return diffX, diffY
    

def warp_points(data, index, transform):
    '''
    Apply the warp to the 'image', to transform in into the 'map'
    '''
    warpedY = transform[0] + data[index,1]*transform[1] + data[index,0]*transform[2] 
    warpedX = transform[3] + data[index,1]*transform[4] + data[index,0]*transform[5]
    
    return warpedY, warpedX


def derive_transform(locs1,
                     locs2,
                     n_trials,
                     sampling_dict): 

    #init warpRMSE
    best_rmse = 99999
    optimal_tp = np.zeros(locs1.shape[0]).astype('bool')
    optimal_cp = np.zeros(locs1.shape[0]).astype('bool')
    trials_to_go = n_trials
    total_trials = 0
      
    # iterate through the numer of trials but
    # only remove the trail if the 
    while trials_to_go:
        
        #select tiepoints from AATSR points (AATSR is the 'image')
        tp_index, cp_index = select_tp(locs1, sampling_dict)
        
            
        # first transform the first coordinate set on to the 
        # second using the selected tie-points.  The screen
        # these tie-opints for outliers.  Re-apply the transform
        # then compute the statistics for the warp                                       
        try: 
            transform = lsfit(locs1, locs2, tp_index)
            # remove outliers from both the tie-points and the control-points
            tp_index = remove_outliers(locs1, locs2, tp_index, transform)
            cp_index = remove_outliers(locs1, locs2, cp_index, transform)
            transform = lsfit(locs1, locs2, tp_index)
            warp_rmse = warp_stats(locs1, locs2, tp_index, cp_index, transform)    
        except Exception:
            total_trials += 1  
            logger.exception('LS fit failed for a trail - continuing')
            
        if warp_rmse < best_rmse:
            optimal_transform = transform
            optimal_tp *= 0   # zero the tie-point index 
            optimal_cp *= 0
            optimal_tp[tp_index] = 1  # set to 1 - most likely a better way to do this
            optimal_cp[cp_index] = 1
            best_rmse = warp_rmse
            
        trials_to_go -= 1
        total_trials += 1
        
        # if we exceed the number of trials by a factor of 2 then stop iterating
        if total_trials == n_trials*2:
            logger.critical('could not find a warp for the given imagery')
            trials_to_go = 0
            
    return optimal_transform, optimal_tp, optimal_cp, best_rmse

#-----------------------------------------------------------------------------------------

def derive(locs1,
           locs2,
           args_dict):
    
    
    print locs1
    print locs1.shape
    n_trials, sampling_dict = estimate_trials(locs1, args_dict)
    logger.info("Trial calculation successful. Running through %i trials" % n_trials)
    transformation, tie_points, check_points, rmse = derive_transform(locs1,
                                                                      locs2,
                                                                      n_trials,
                                                                      sampling_dict)
    return transformation, tie_points, check_points, rmse