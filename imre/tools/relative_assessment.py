import sys
import os
import h5py
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap 
import numpy as np
import cv2
from numpy.ma import masked_array
import scipy.ndimage as nd
import datetime


def appendimages(im1,im2):
    """ Return a new image that appends the two images side-by-side. """
    # select the image with the fewest rows and fill in enough empty rows
    rows1 = im1.shape[0]
    rows2 = im2.shape[0]
    if rows1 < rows2:
        im1 = np.concatenate((im1,np.zeros((rows2-rows1,im1.shape[1]))),axis=0)
    elif rows1 > rows2:
        im2 = np.concatenate((im2,np.zeros((rows1-rows2,im2.shape[1]))),axis=0)
    # if none of these cases they are equal, no filling needed.
    return np.concatenate((im1,im2), axis=1)


def plot_color(m,lat,lon,data,name,time,path):
    m.pcolormesh(lon, lat, data, vmin=-2, vmax=2, latlon=True)
    cbar = plt.colorbar()
    m.drawparallels(np.arange(-90,90,20))
    m.drawmeridians(np.arange(0,360,20))
    m.drawcoastlines()
    plt.savefig(path+name+"/"+time+".png",bbox_inches='tight')
    plt.close()


def hist_eq(im,nbr_bins=256):
    imhist,bins = np.histogram(im.flatten(),nbr_bins,normed=True)
    cdf = imhist.cumsum() #cumulative distribution function
    cdf = 255 * cdf / cdf[-1] #normalize
    im2 = np.interp(im.flatten(),bins[:-1],cdf)
    return im2.reshape(im.shape).astype('uint8')


"""
def remove_outliers(features, tracked_features):
    
    flow_y = features[] - features[]
    flow_x = features[] - features[]
    
    iterate = True
    old_change = 0
    counter = 0 
    while iterate:
        mean_flow = np.mean(flow_y[cma_y]) 
        std_flow = np.std(flow_y[cma_y])
        # outliers are where flow > 3 std and there is no cloud
        outliers = (np.abs(flow_y) > (mean_flow + 3*std_flow)) & cma_y
        flow_y = flow_y[~outliers]
        # check how many mask points are chaning to decide convergence
        new_change = np.sum(outliers) 
        no_change = np.abs(old_change - new_change) < 10
        old_change = new_change
        counter += 1
        if no_change | (counter >= 15):
            iterate = False
    
    iterate = True
    old_change = 0
    counter = 0 
    while iterate:
        mean_flow = np.mean(flow_x[cma_x]) 
        std_flow = np.std(flow_x[cma_x])
        # outliers are where flow > 3 std and there is no cloud
        outliers = (np.abs(flow_x) > (mean_flow + 3*std_flow)) & cma_x
        flow_x = flow_x[~outliers]
        
        # check how many mask points are chaning to decide convergence
        new_change = np.sum(outliers) 
        no_change = np.abs(old_change - new_change) < 10
        old_change = new_change
        counter += 1
        if no_change | (counter >= 15):
            iterate = False
    
    return flow_y, flow_x, cma_y, cma_x
"""


def plot_matches(im1,im2,locs1,locs2,show_below=False, pltLim=100):
    """ Show a figure with lines joining the accepted matches
    input: im1,im2 (images as arrays), locs1,locs2 (feature locations),
            matchscores (as output from match()),
    show_below (if images should be shown below matches). """
    
    def convert_to_grayscale(image):
        if np.min(image == 0) and np.max(image == 255):
            print 'image already grayscale'
            return image
        else:
            minPixelValue = np.min(image) * 1.0
            maxPixelValue = np.max(image) * 1.0
            imageGrayScale = ((image + minPixelValue)/maxPixelValue)*255
            return imageGrayScale
    
    im3 = appendimages(convert_to_grayscale(im1),convert_to_grayscale(im2))
    if show_below:
        im3 = np.vstack((im3,im3))
        
    plt.imshow(im3, cmap="gray")
    
    cols1 = im1.shape[1]
    count = 0
    for i in xrange(locs1.shape[0]):
        if count > pltLim: continue 
        plt.plot([locs1[i,0],locs2[i,0]+cols1],[locs1[i,1],locs2[i,1]],'c')
        plt.axis('off')
        count += 1
    
    plt.show()
    plt.close()


def time_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end
    

def remove_outliers(data):
    """Just do a couple of iterations"""
    
    for iteration in [0,1,2,3,4,5]:
        mean = np.mean(data) 
        std = np.std(data)
        # outliers are where flow > 3 std and there is no cloud
        outliers = (np.abs(data) > (mean + 3*std))
        
        # check how many mask points are chaning to decide convergence
        data = data[~outliers] 
    return data


def draw_maps(m, lat, lon, flow_y, cma_y, flow_x, cma_x, time, out_path):
    
    #do the plotting
    plot_color(m,lat,lon,masked_array(flow_y, ~cma_y), "flow_y", time, out_path)
    plot_color(m,lat,lon,masked_array(flow_x, ~cma_x), "flow_x", time, out_path)

def main():
    
    # params for ShiTomasi corner detection
    feature_params = dict(maxCorners = 100,
                          qualityLevel = 0.1,
                          minDistance = 3,
                          blockSize = 3)

    # Parameters for lucas kanade optical flow
    lk_params = dict(winSize  = (7,7),
                    maxLevel = 1,
                    criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
    
    # set up data paths
    sev_path = "/Users/dnf/Projects/KCL-seviri-georef/data/seviri/msg_1/"
    out_path = "/Users/dnf/Projects/KCL-seviri-georef/data/outputs/"
    
    # read in geographic data for plotting purposes
    #sev_lats = h5py.File(geo_path + "HDF5_LSASAF_MSG_LAT_"+region+"_201302131200")["LAT"][:] / 100.0
    #sev_lons = h5py.File(geo_path + "HDF5_LSASAF_MSG_LON_"+region+"_201302131200")["LON"][:] / 100.0

    # set up the basemap
    #m = Basemap(projection='geos',lon_0=-0,resolution='l')
    

    # make a list of the seviri datasets
    sev_data_list = []
    for sev_data in os.listdir(sev_path):
                
        sev_data_list.append(sev_data)

    # now loop over the seviri data list and match outputs
    y_mean, y_med, y_mad, y_std = [],[],[],[]
    x_mean, x_med, x_mad, x_std = [],[],[],[]
    # start from 1 to miss ds_store
    for i in xrange(1,len(sev_data_list),2):
        sev_data_a = sev_data_list[i]
        sev_data_b = sev_data_list[i+1]
                
        # read in the two data sets
        Channel = "Channel 12"  # "Channel 04"
        sev_data_a = h5py.File(sev_path + sev_data_a)["U-MARF"]["MSG"]["Level1.5"]["DATA"][Channel]["IMAGE_DATA"][1200:1300,2600:2900]
        sev_data_b = h5py.File(sev_path + sev_data_b)["U-MARF"]["MSG"]["Level1.5"]["DATA"][Channel]["IMAGE_DATA"][1200:1300,2600:2900]
        
        # Now for the relative assessments subset the data so that it is only over switzerland.  Just
        # trying to replicate the other findings here!
        
        
        # histogram equalise the data
        sev_data_a_eq = np.array(hist_eq(sev_data_a))
        sev_data_b_eq = np.array(hist_eq(sev_data_b))
        
        plt.imshow(sev_data_a_eq, cmap="gray")
        plt.savefig("/Users/dnf/Desktop/sev_data_a_eq.png",bbox_inches="tight")
        plt.close()
        plt.imshow(sev_data_b_eq, cmap="gray")
        plt.savefig("/Users/dnf/Desktop/sev_data_b_eq.png",bbox_inches="tight")
        plt.close()
        
        # get the optical flow using Farneback algorithm
        #flow = cv2.calcOpticalFlowFarneback(sev_data_b_eq, sev_data_a_eq, 0.5, 4, 7, 7, 7, 1.5, cv2.OPTFLOW_FARNEBACK_GAUSSIAN)
        features = cv2.goodFeaturesToTrack(sev_data_a_eq, 
                                           #mask = cma.astype("uint8"),
                                           **feature_params)
        tracked_features, status, tracking_error = cv2.calcOpticalFlowPyrLK(sev_data_a_eq, 
                                                                            sev_data_b_eq, 
                                                                            features, 
                                                                            None,
                                                                            **lk_params)
        
        plot_matches(sev_data_a_eq,sev_data_b_eq,np.squeeze(features),np.squeeze(tracked_features))
        
        # compute flow
        try:
            y_flow = features.squeeze()[:,1] - tracked_features.squeeze()[:,1]
            x_flow = features.squeeze()[:,0] - tracked_features.squeeze()[:,0]
        except:
            continue
                
        # remove outliers
        y_flow = remove_outliers(y_flow)
        x_flow = remove_outliers(x_flow)
        
        # compute and append stats
        y_mean.append(np.mean(y_flow))
        y_mad.append(np.mean(np.abs(y_flow - np.mean(y_flow))))
        y_std.append(np.std(y_flow))
        y_med.append(np.median(y_flow))
        
        x_mean.append(np.mean(x_flow))
        x_mad.append(np.mean(np.abs(x_flow - np.mean(x_flow))))
        x_std.append(np.std(x_flow))
        x_med.append(np.median(x_flow))
                
        # write out the image
        #draw_maps(m, sev_lats, sev_lons, flow_y, cma_y, flow_x, cma_x, time, out_path)
        
    print "min y_mean", np.min(y_mean)
    print "max y_mean", np.max(y_mean)
    print "mean y_mean", np.mean(y_mean)
    print ""
    print "min y_std", np.min(y_std)
    print "max y_std", np.max(y_std)
    print "mean y_std", np.mean(y_std)
    print ""
    print "min y_med", np.min(y_med)
    print "max y_med", np.max(y_med)
    print "mean y_med", np.mean(y_med)
    print ""
    print "min y_mad", np.min(y_mad)
    print "max y_mad", np.max(y_mad)
    print "mean y_mad", np.mean(y_mad)
    print ""
    print "min x_mean", np.min(x_mean)
    print "max x_mean", np.max(x_mean)
    print "mean x_mean", np.mean(x_mean)
    print ""
    print "min x_std", np.min(x_std)
    print "max x_std", np.max(x_std)
    print "mean x_std", np.mean(x_std)
    print ""
    print "min x_med", np.min(x_med)
    print "max x_med", np.max(x_med)
    print "mean x_med", np.mean(x_med)
    print ""
    print "min x_mad", np.min(x_mad)
    print "max x_mad", np.max(x_mad)
    print "mean x_mad", np.mean(x_mad)
    print ""

    
if __name__ == "__main__":
    sys.exit(main())