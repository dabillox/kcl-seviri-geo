"""
.. module:: register
   :platform: mac, linux
   :synopsis: gets the reference image against which
   following collocations can be run

.. moduleauthor:: Daniel Fisher <daniel.fisher@kcl.ac.uk>

"""
import logging
logger = logging.getLogger(__name__)

import imre.register.assess as assess
import imre.constants.proc_const as constants

import numpy as np

class ReferenceImageFinder(object):
    """Contains methods to find a suitable reference
    image against which further comparisons can be made 
    """
    
    def __init__(self, image_paths, cm_paths):
        """A class to select a reference image.

        Args:
           images (list): a list of images  
    .
        """              
        self.image_paths = image_paths
        self.cm_paths = cm_paths
        self.r_image_count = range(constants.N_REFERENCE-1)
        self.c_image_count = range(1,constants.N_REFERENCE)
        self.array = np.zeros([constants.N_REFERENCE, constants.N_REFERENCE])
        
    def _choose_candidates(self):
        """defines the candidate image paths"""
        n_candidates = constants.N_REFERENCE * 2
        
        image_paths = self.image_paths[0:n_candidates]
        cm_paths = self.cm_paths[0:n_candidates]
        
        image_times = sorted([item[-12:] for item in image_paths])
        cm_times = sorted([item[-12:] for item in cm_paths])
        shared_times = [im_time for im_time, cm_time 
                        in zip(image_times, cm_times)
                        if im_time == cm_time]
        
        if len(shared_times) < constants.N_REFERENCE:
            logger.critical("Too few reference images retained after comparing shared times")
        
        image_paths = [item for item in image_paths if item[-12:] in shared_times]
        cm_paths = [item for item in cm_paths if item[-12:] in shared_times]
        
        self.image_paths = sorted(image_paths[0:constants.N_REFERENCE])
        self.cm_paths = sorted(cm_paths[0:constants.N_REFERENCE])

    def _compare_candidates(self):
        """Compares an image with all other images."""
        for i in self.r_image_count:
            self._do_comparison(i)
            self.c_image_count = self.c_image_count[i+1:]
            
    def _do_comparison(self, i):
        """Does the actual comparison."""
        for j in self.c_image_count:
            magnitude = assess.ImageShiftFinder(self.image_paths[i], 
                                                self.image_paths[j],
                                                self.cm_paths[i],
                                                self.cm_paths[j]).get_magnitude()
            self.array[i,j] = magnitude
            
        
    def _select_reference(self):
        self.array += self.array.T
        mean_magnitudes = np.sum(self.array, 1) / (constants.N_REFERENCE - 1)
        for i, mm in enumerate(mean_magnitudes):
            if mm < constants.RELATIVE_ERROR_LIMIT:
                self.reference_image = self.image_list[i]
                break
        self.reference_image = None
    
    def find(self): 
        self._choose_candidates()
        self._compare_candidates()
        self._select_reference()
        return self.reference_image
    