# -*- coding: utf-8 -*-
"""
.. module:: xm
   :platform: mac, linux
   :synopsis: performs the feature extraction, matching
   and cleaning for a given pair of input arrays

.. moduleauthor:: Daniel Fisher <daniel.fisher@kcl.ac.uk>

"""

import logging
logger = logging.getLogger(__name__)

import extractfeatures
import matchfeatures

import numpy as np

#
# At soem point make a function to check all the input
# data and arguments, so move these functions out of
# here.
#

def _inputs_exist(data_dict):
    '''
    Raise an exception of the required inputs are not present
    '''
    for k in ['ref_arr', 'comp_arr']:
        try:
            data_dict[k]
        except:
            logger.exception(k + ' not defined in data_dict')
            raise


def _is_array(data_dict):
    """
    Check if the input data is an array.  Try and 
    convert to np array.  Raise exception if not possible
    """
    
    for k in ['ref_arr', 'comp_arr']:
        if not isinstance(data_dict[k], np.ndarray):
            try:
                data_dict[k] = np.array(data_dict[k])
            except:
                logger.exception(k + ' array not of numpy type and could not convert')
                raise
            

def _is_2d(data_dict):
    """
    Check if the input data is 2D.
    """
    for k in ['ref_arr', 'comp_arr']:
        if len(data_dict[k].shape) != 2:
            logger.exception(k + 'is not a 2D image array')
            raise
        

def _get_params(args_dict):
    '''
    Set up SIFT processing parameters
    '''
    params = (" --edge-thresh " + args_dict['EDGE_THRESH'] + 
              " --peak-thresh " + args_dict['PEAK_THRESH'] + 
              " --octaves " + args_dict['OCTAVES'])
    return params
    

def extract_match(data_dict,
                  args_dict):
    """
    Extracts, matches and cleans the features from the 
    imagery passed to the python script.
    
    kwargs:
        
    Returns:
    
    Raises:
    """
    # set up the processing params
    params = _get_params(args_dict)
    
    # extract the features
    extractfeatures.extract(data_dict, 
                            params)
    
    # match the features
    ref_locs, comp_locs = matchfeatures.match(data_dict,
                                              args_dict)
        
    return ref_locs, comp_locs