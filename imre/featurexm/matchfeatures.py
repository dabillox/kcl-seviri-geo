"""
.. module:: match_features
   :platform: mac, linux
   :synopsis: match the SIFT features

.. moduleauthor:: Daniel Fisher <daniel.fisher@kcl.ac.uk>


x, y = np.hsplit(lr - lc,2)
    x_shifts = x
    y_shifts = y

"""

import logging
logger = logging.getLogger(__name__)

import numpy as np
import scipy.linalg as linalg


def _load_features():
    """ Read feature properties and return in matrix form. """
    fr = np.loadtxt('../temp/ref.sift')
    fc = np.loadtxt('../temp/comp.sift')
    lr = fr[:,:4] # x, y, scale, orientation
    dr = fr[:,4:]
    lc = fc[:,:4]
    dc = fc[:,4:]
    
    logger.debug("Number of reference features before masking: %d" % lr.shape[0])
    logger.debug("Number of comparison features before masking: %d" % lc.shape[0])
    
    return lr, dr, lc, dc
     

def _mask_features(lr, dr, lc, dc, data_dict):
    """mask off features"""
    
    ref_mask = data_dict['ref_mask']
    comp_mask = data_dict['comp_mask']
    
    r_index = ref_mask[lr[:,1].astype("int"),lr[:,0].astype("int")]
    lr = lr[r_index, :]
    dr = dr[r_index, :] 
    
    c_index = comp_mask[lc[:,1].astype("int"),lc[:,0].astype("int")]
    lc = lc[c_index, :]
    dc = dc[c_index, :]
    
    logger.debug("Number of reference features after masking: %d" % lr.shape[0])
    logger.debug("Number of comparison features after masking: %d" % lc.shape[0])
    
    return lr, dr, lc, dc


def _match_twosided(dr, dc, DIST_RATIO):
    """ Two-sided symmetric version of match(). """
    
    matches_12 = _match(dr, dc, DIST_RATIO)
    matches_21 = _match(dc, dr, DIST_RATIO)
    ndx_12 = matches_12.nonzero()[0]
    
    # remove matches that are not symmetric
    for n in ndx_12:
        if matches_21[int(matches_12[n])] != n:
            matches_12[n] = 0
    return matches_12


def _match( desc_a, desc_b, DIST_RATIO):
    """ For each descriptor in the first image,
    select its match in the second image.
    input: desc1 (descriptors for the first image),
    desc2 (same for second image). """
    desc_a = np.array([d/linalg.norm(d) for d in desc_a])
    desc_b = np.array([d/linalg.norm(d) for d in desc_b])
    desc_a_size = desc_a.shape
    matchscores = np.zeros((desc_a_size[0],1),'int')
    desc_b_t = desc_b.T  # precompute matrix transpose
    for i in range(desc_a_size[0]):
        dotprods = np.dot(desc_a[i,:], desc_b_t)  # vector of dot products
        dotprods = 0.9999*dotprods
        # inverse cosine and sort, return index for features in second image
        indx = np.argsort(np.arccos(dotprods))
        
        # check if nearest neighbor has angle less than dist_ratio times 2nd
        if np.arccos(dotprods)[indx[0]] < DIST_RATIO * np.arccos(dotprods)[indx[1]]:
            matchscores[i] = int(indx[0])
    return matchscores
    
    
def _extract_matches(lr, dr, lc, dc, matches):
    matches = np.squeeze(matches)
    match_index = matches != 0
    logger.debug("Number of matched features: %d" % np.sum(match_index))
    lr = lr[match_index,0:2]
    lc = np.squeeze(lc[matches[match_index], 0:2])
    return lr, lc
    


def match(data_dict,
          args_dict):
    '''
    Matches features based on the description and returns
    the coordinates for the reference (r) and comparison 
    (c) images.
    
    Arrangement of lr and lc (the location vectors) is x,y
    '''
    
    DIST_RATIO = args_dict['DIST_RATIO']
    
    lr, dr, lc, dc = _load_features()
    lr, dr, lc, dc = _mask_features(lr, dr, lc, dc, data_dict)
    matches = _match_twosided(dr, dc, DIST_RATIO)
    lr, lc = _extract_matches(lr, dr, lc, dc, matches)
    return lr, lc