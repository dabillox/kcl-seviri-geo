"""
.. module:: extract_features
   :platform: mac, linux
   :synopsis: using the VLfeat sift algorithm features are 
   extracted from a given input image

.. moduleauthor:: Daniel Fisher <daniel.fisher@kcl.ac.uk>

"""

import logging
logger = logging.getLogger(__name__)

import numpy as np
import subprocess
from PIL import Image
from sys import platform as _platform

import os


def _convert_to_grayscale(arr):
    '''
    Converts an image to 8 bit
    '''
    if np.nanmin(arr == 0) and np.nanmax(arr == 255):
            return arr
    else:
        min_pixel_value = np.nanmin(arr) * 1.0
        if min_pixel_value < 0:
            logger.warning("Converting to grayscale.  Min pixel < 0 encountered " +
                           "setting min pixel value and pixel < 0 to 0")
            min_pixel_value = 0.
            arr[arr < 0] = 0.
        max_pixel_value = np.nanmax(arr) * 1.0
        image_grayscale = ((arr + min_pixel_value) / max_pixel_value) * 255
        return image_grayscale


def _write_to_pgm(arr, pgm_path):
    '''
    Write as pgm
    '''
    arr = Image.fromarray(np.uint8(arr))
    arr.save(pgm_path)


def _sift_process(path_to_feat, 
                  pgm_path, 
                  exe_dict,
                  params):
    '''
    Call SIFT feature detector.
    '''
    output = ' --output=' + path_to_feat
    p = subprocess.call([exe_dict[_platform] + \
                        pgm_path + \
                        output + \
                        params], 
                        shell=True)
    
    
def extract(data_dict,
            params):
    '''
    Performs the feature extraction
    '''

    logger.debug("Running feature extraction")
    
    # you touch, you break (probably :) )  
    print os.getcwd()  
    path_to_pgm = "../temp/tmp.pgm"
    path_to_feat = {'ref_arr': '../temp/ref.sift',
                    'comp_arr': '../temp/comp.sift'}
    exe_dict = {"linux": '../soft/vlfeat-0.9.19/bin/glnxa64/sift ',
                "linux2": '../soft/vlfeat-0.9.19/bin/glnxa64/sift ',
                "darwin": '../soft/vlfeat-0.9.19/bin/maci64/sift '}

    # Lets do it 
    for k in ['ref_arr', 'comp_arr']:
        #arr = _histeq(data_dict[k])
        arr = _convert_to_grayscale(data_dict[k])

        #arr = _convert_to_grayscale(arr)
        _write_to_pgm(arr, path_to_pgm)
        _sift_process(path_to_feat[k],
                      path_to_pgm,
                      exe_dict,
                      params)