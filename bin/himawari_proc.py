'''
Created on June 24, 2015

@author: dnf
'''

import logging
logger = logging.getLogger(__name__)


import imre.featurexm.xm as xm
import imre.imwarper.warp as warp
import imre.analyse.plotter as plotter

import numpy as np
import scipy.ndimage as ndimage
import scipy.linalg as linalg
from sys import platform as _platform
import os

import matplotlib.pyplot as plt


def read_config_file():
    pass


def img_read(filename):
    """
    read in the ENVI image file (Binary)
    put them in a data dictionary
    """
    if not os.path.exists(filename):
        print 'can not read %s' % filename
    file_basename = os.path.basename(filename)
    file_basename = os.path.splitext(file_basename)[0]
    slot_time = file_basename.split("_")[-1]

    #d = {}
    dim = 5500 #hard coded for Himawari8
    logger.debug('reading file %s' % file)

    dtype = np.float32
    shape = (dim, dim)
    data = np.fromfile(filename, dtype=dtype).reshape(shape)
    #data_key = slot_time
    #d[data_key] = data.astype(dtype)

    return data


def get_error(locs1, locs2):
    diff_x = locs1[:,0] - locs2[:,0]
    diff_y = locs1[:,1] - locs2[:,1]
    residuals = np.sqrt(diff_y**2 + diff_x**2)
    sumSquareResiduals  = np.sum(residuals**2)
    return np.sqrt(sumSquareResiduals/diff_y.shape[0])


def main():
    '''
    Script to process Himawari data as test for Weidong
    '''

    # set logging level
    logging.basicConfig(level=logging.DEBUG)
    print logging.getLogger().getEffectiveLevel()

    # lets read in the himawari data
    path = '/Volumes/INTENSO/data/KCL/KCL-seviri-geo/data/himawari/H8_data_for_Dan/'
    f1 = 'H8_dif_201501080540.img'
    f2 = 'H8_dif_201501080550.img'
    rad1 = img_read(path + f1)
    rad2 = img_read(path + f2)

    # lets just make a circular mask for himarwari to get rid of all the
    # crap round the edges
    r = 2750
    y,x = np.ogrid[-r:r, -r:r]
    circle_mask = x*x + y*y <= ((r-100)*(r-100))

    rad1_mask = rad1 < 15
    rad2_mask = rad2 < 15

    #plt.imshow(circle_mask & rad1_mask, cmap='gray')
    #cbar = plt.colorbar()
    #plt.show()

    cm1 = circle_mask & rad1_mask
    cm2 = circle_mask & rad2_mask
    lats = None
    lons = None

    data_dict = {'ref_arr': rad1,
                  'comp_arr': rad2,
                  'ref_mask': cm1,
                  'comp_mask': cm2,
                  'lats':lats,
                  'lons':lons}

    # set up the arguments dict
    args_dict = {}
    args_dict['EDGE_THRESH'] = "5"
    args_dict['PEAK_THRESH'] = "2"
    args_dict['OCTAVES'] = "2"
    args_dict['DIST_RATIO'] = 0.6
    args_dict['NULL_VALUE'] = 0
    args_dict['TRIALS'] = 250
    args_dict['MAX_BUCKET_SIZE'] = 1024
    args_dict['TRIAL_PROBABILITY'] = 0.01
    args_dict['BUCKET_INIT_SIZE'] = 4

    # extract and clean the matches and get initial RMSE
    ref_locs, comp_locs = xm.extract_match(data_dict,
                                           args_dict)

    rmse = get_error(ref_locs, comp_locs)


    # if the shift is greater than the relative error then warp
    # and get the corrected RMSE
    warped_comp, warped_mask, tie_points, check_points, new_rmse = warp.derive_apply(comp_locs,
                                                                                     ref_locs,
                                                                                     data_dict,
                                                                                     args_dict)

    logger.info("RMSE Prior to warp = %f" % (rmse))
    logger.info("RMSE Following warp = %f" % (new_rmse))

    data_dict['warped_comp'] = warped_comp


    # get the level of the logger: Critical = 50;
    # Error = 40; WArning = 30; Info = 20; Debug = 10;
    # notset = 0
    lev = logging.getLogger().getEffectiveLevel()
    if lev == 10:  # debug level
        plotter.make_plots(ref_locs,
                           comp_locs,
                           data_dict,
                           warped_comp,
                           tie_points,
                           check_points)

if __name__ == '__main__':
    main()
