'''
Created on Feb 24, 2015

@author: dnf
'''

import logging
logger = logging.getLogger(__name__)


import imre.featurexm.xm as xm
import imre.imwarper.warp as warp
import imre.analyse.plotter as plotter

import numpy as np
import scipy.ndimage as ndimage
import scipy.linalg as linalg
from sys import platform as _platform
import h5py


def read_config_file():
    pass


def rotate_translate(img, mask=False):

    centre=0.5*np.array(img.shape)
    a=25*np.pi/180.0
    rot=np.array([[np.cos(a),np.sin(a)],[-np.sin(a),np.cos(a)]])
    if mask:
        output=np.bool
    else:
        output=np.float
    rotimg=ndimage.interpolation.affine_transform(img,rot,order=0,offset=5,cval=0.0,output=output)
    return rotimg

def read_h5(image_path):
    '''
    Read in the required H5 data.
    '''
    data = h5py.File(image_path)
    if "RAD" in image_path:
        data = data["RADIANCE"]
        return data[:] / data.attrs["SCALING_FACTOR"] + data.attrs["OFFSET"]
    elif "TB" in image_path:
        data = data["Brightness Temperature"]
        return data[:] / data.attrs["SCALING_FACTOR"] + data.attrs["OFFSET"]
    elif "CMa" in image_path:
        data = data["CMa"]
        return data[:] / data.attrs["SCALING_FACTOR"] + data.attrs["OFFSET"]
    elif "LAT" in image_path:
        data = data["LAT"]
        return data[:] / data.attrs["SCALING_FACTOR"] + data.attrs["OFFSET"]
    elif "LON" in image_path:
        data = data["LON"]
        return data[:] / data.attrs["SCALING_FACTOR"] + data.attrs["OFFSET"]
    else:
        logger.CRITICAL("cannot read HDF5 dataset")

def get_error(locs1, locs2):
    diffX = locs1[:,0] - locs2[:,0]
    diffY = locs1[:,1] - locs2[:,1]
    residuals = np.sqrt(diffY**2 + diffX**2)
    sumSquareResiduals  = np.sum(residuals**2)
    return np.sqrt(sumSquareResiduals/diffY.shape[0])


def main():
    '''
    Script to process SEVIRI data to test coregistration
    qaulity and improve it if required
    '''
    config = {}
    execfile("proc_v0_2.conf", config)

    # set logging level
    level = logging.getLevelName(config['debug_level'])
    logging.basicConfig(level)

    # set file paths
    data_path =
    mask_path =
    geo_path =

    # for now have the processing details here, but later put them into a config file!

    if _platform == "linux" or _platform == "linux2":
        PATH = '/media/daniel/INTENSO1/data/KCL/KCL-seviri-geo/data/seviri/msg_3/full_disk_test/'
    elif _platform == "darwin":
        PATH = ''

    lat_name = 'HDF5_LSASAF_MSG_LAT_MSG-Disk_201408100000'
    lon_name = 'HDF5_LSASAF_MSG_LON_MSG-Disk_201408100000'
    lats = read_h5(PATH + lat_name)
    lons = read_h5(PATH + lon_name)

    rad1_name = 'HDF5_006_MSG_RAD_MSG-Disk_201502261230'
    rad2_name = 'HDF5_006_MSG_RAD_MSG-Disk_201502261245'
    rad1 = read_h5(PATH + rad1_name)
    rad2 = read_h5(PATH + rad2_name)

    cm1_name = 'HDF5_MSG_NWC_CMa_MSG-Disk_201502261230'
    cm2_name = 'HDF5_MSG_NWC_CMa_MSG-Disk_201502261245'
    cm1 = read_h5(PATH + cm1_name)
    cm2 = read_h5(PATH + cm2_name)
    cm1 = cm1 == 1
    cm2 = cm2 == 1

    struc = np.ones((7,7))
    cm1 = ndimage.morphology.binary_erosion(cm1, structure=struc)
    cm2 = ndimage.morphology.binary_erosion(cm2, structure=struc)

    rad2 = rotate_translate(rad2)
    cm2 = rotate_translate(cm2,  mask=True)

    data_dict = {'ref_arr': rad1,
                  'comp_arr': rad2,
                  'ref_mask': cm1,
                  'comp_mask': cm2,
                  'lats':lats,
                  'lons':lons}

    # set up the arguments dict
    args_dict = {}
    args_dict['EDGE_THRESH'] = "5"
    args_dict['PEAK_THRESH'] = "2"
    args_dict['OCTAVES'] = "2"
    args_dict['DIST_RATIO'] = 0.6
    args_dict['NULL_VALUE'] = 0
    args_dict['TRIALS'] = 250
    args_dict['MAX_BUCKET_SIZE'] = 1024
    args_dict['TRIAL_PROBABILITY'] = 0.01
    args_dict['BUCKET_INIT_SIZE'] = 4

    # extract and clean the matches and get initial RMSE
    ref_locs, comp_locs = xm.extract_match(data_dict,
                                           args_dict)

    rmse = get_error(ref_locs, comp_locs)


    # if the shift is greater than the relative error then warp
    # and get the corrected RMSE
    warped_comp, warped_mask, tie_points, check_points, new_rmse = warp.derive_apply(comp_locs,
                                                                                     ref_locs,
                                                                                     data_dict,
                                                                                     args_dict)

    logger.info("RMSE Prior to warp = %f" % (rmse))
    logger.info("RMSE Following warp = %f" % (new_rmse))

    data_dict['warped_comp'] = warped_comp


    # get the level of the logger: Critical = 50;
    # Error = 40; WArning = 30; Info = 20; Debug = 10;
    # notset = 0
    lev = logging.getLogger().getEffectiveLevel()
    if lev == 10:  # debug level
        plotter.make_plots(ref_locs,
                           comp_locs,
                           data_dict,
                           warped_comp,
                           tie_points,
                           check_points)

if __name__ == '__main__':
    main()
