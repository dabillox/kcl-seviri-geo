try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Image Co-registration package, based on the SIFT algorithm',
    'author': 'Daniel Fisher',
    'url': '',
    'download_url': '',
    'author_email': 'daniel.fisher@kcl.ac.uk',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['imre'],
    'scripts': [''],
    'name': 'kcl-serviri-geo'
}

setup(**config)